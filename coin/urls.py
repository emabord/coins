from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from money.views import MoneyListView

urlpatterns = [
    url(
        r'^admin/',
        admin.site.urls),

    url(
        r'^api/',
        include('money.urls')),

    #provisorio
    url(
        r'$',
        MoneyListView.as_view(),
        name="index"),
]
