# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.generic import ListView
from django.db.models.query import QuerySet
import json


class JSONResponseMixin(ListView):
    """Summary.
    A mixin that can be used to render a JSON response.
    It can return JSON data, from post and get requests
    """
    message_error = {"message": "The error is not defined"}
    '''You can change this method in the model, as_json is the default one'''
    method_json = "as_json"
    parameter_method_json = None
    code = None

    def send_response(self, response):
        response = json.dumps(response, indent=4, separators=(',', ': '))
        return HttpResponse(
            response,
            content_type="application/json")

    def get_collection(self, query):
        response = [
            self.method_execute(element)
            for element in query
        ]
        return response

    def get(self, request, *args, **kwargs):
        query = self.get_query(request, *args, **kwargs)
        if not isinstance(query, QuerySet):
            return self.error()
        response = self.get_collection(query)
        return self.send_response(response)

    def get_post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return [form.instance.as_json()]
        return [dict(form.errors.items())]

    def post(self, request, *args, **kwargs):
        response = self.get_post(request)
        return self.send_response(response)
    """
            --- Hooks ---
    @get_query  >>> Filter the objects models and return a QuerySet Object
    #message_error      >>> String for render the error message
    """

    """
    Dont forget that @get_query method must return a QuerySet Object
    """

    def get_query(self, request=None, *args, **kwargs):
        return self.get_queryset()

    """
    You can change the message_error
    """

    def error(self):
        return HttpResponse(
            self.message_error,
            content_type="application/json",
            status=self.code)

    def method_execute(self, element):
        method = getattr(element, self.method_json)
        return method(self.parameter_method_json)
