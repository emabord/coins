from django.db.models import Q
import operator
import json
from django.http import HttpResponse


class DatatableAsync:

    def __init__(
        self, model,
        fields_orders,
        fields_filters=None,
        extra_filter=None,
        exclude_filter=None,
        method_json=None
    ):
        self.model = model
        self.fields_orders = [{"asc": field, "desc": "-" + field}
                              for field in fields_orders]
        self.fields_filters = fields_filters
        self.start_query = "iDisplayStart"
        self.length_data = "iDisplayLength"
        self.order_name = "iSortCol_0"
        self.column_name = "sSortDir_0"
        self.search_name = "sSearch"
        self.size = 0
        self.result = 0
        self.html = None
        self.extra_filter = extra_filter
        self.query = False
        self.echo = 'sEcho'
        self.sSearch_ = "sSearch_"
        self.filter_date_name = ""
        self.filter_date_value = ""
        self.exclude_filter = exclude_filter
        self.method_json = method_json
        self.parameter = None

    def columns_filter(self, request):
        col = 0
        for i in self.fields_orders:
            if request.GET["sSearch_" + str(col)]:
                return True
            col += 1
        return False

    def get_columns_filters(self, request):
        filters = {}
        col = 0
        for i in self.fields_orders:
            if request.GET[self.sSearch_ + str(col)]:

                if "\\" in request.GET["sSearch_" + str(col)][1:-1]:
                    fecha = request.GET[
                        "sSearch_" + str(col)][1:-1].split("\\")
                    filters[i['asc']] = fecha[0] + fecha[1] + fecha[2]
                else:
                    filters[i['asc']] = request.GET[
                        "sSearch_" + str(col)][1:-1]
            col += 1
        return filters

    def get_result(self, request):
        start = int(request.GET[self.start_query])
        length = int(request.GET[self.length_data])
        order = self.fields_orders[int(request.GET[self.order_name])][
            request.GET[self.column_name]]
        search = request.GET[self.search_name]
        if search:
            if self.query:
                objects = self.model.filter_all(search)
            else:
                data_q = []
                for field in self.fields_filters:
                    data_q.append(Q(**{field + '__icontains': search}))
                objects = self.model.objects.filter(
                    reduce(operator.or_, data_q)).order_by(order)
        else:
            objects = self.model.objects.order_by(order)
        if self.columns_filter(request):
            objects = objects.filter(
                **self.get_columns_filters(request)).order_by(order)
        if self.extra_filter:
            objects = objects.filter(**self.extra_filter)
        if self.filter_date_value:
            objects = objects.filter(
                **{self.filter_date_name: self.filter_date_value})
        if self.exclude_filter:
            try:
                objects = self.model.exclude_filter(objects)
            except(AttributeError):
                objects = objects.exclude(**self.exclude_filter)
        self.size = objects.count()
        self.result = objects[start:start + length]
        return self.json(request)

    def json(self, request):
        data = self.data()
        data_json = {
            "data": data,
            "draw": request.GET[self.echo],
            "recordsTotal": self.size,
            "recordsFiltered": self.size
        }
        response = json.dumps(data_json, indent=4, separators=(',', ': '))
        return HttpResponse(response, content_type="application/json")

    def data(self):
        if self.method_json:
            data = [
                getattr(dato, self.method_json)(self.parameter)
                for dato in self.result]
            return data

        """ method abstract example:"""
        """
        if self.html:
            data = [
                    u.username,
                    u.email,
                    u.first_name + " " + u.last_name,
                    (
                        self.html.construct(u)
                    )
                    for u in self.result]
        else:
            data = [
                    u.username,
                    u.email,
                    u.first_name + " " + u.last_name
                     for u in self.result]
        return data
        """
        pass
