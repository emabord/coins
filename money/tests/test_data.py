

class TestDataMoney:

    money_data = {"name": "test", "symbol": "$$"}
    money_data_without_symbol = {"name": "test"}
    money_data_without_name = {"symbol": "$$"}
    money_data_update = {"name": "test", "symbol": "usd"}

    symbol_obligatorie = [{
        "symbol": ["Este campo es obligatorio."]
    }]

    name_obligatorie = [{
        "name": ["Este campo es obligatorio."]
    }]

    obligatories = [
        {
            "symbol": [
                "Este campo es obligatorio."
            ],
            "name": [
                "Este campo es obligatorio."
            ]
        }
    ]
    parameters = {'money_id': 1}
