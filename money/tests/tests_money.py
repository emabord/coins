# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse
from django.test import TestCase
from django.utils.encoding import force_text

from django.contrib.auth.models import User
from ..models import Money
from .test_data import TestDataMoney
from django.contrib.auth.models import Permission


class TestMixinSetUp:

    def setUp(self):
        user = User.objects.create_user(
            'temporary', 'temp@temp.com', 'temporary')
        can_list_view = Permission.objects.get(codename='view_money_list')
        can_change_status = Permission.objects.get(
            codename='change_money_status')
        user.user_permissions.add(can_list_view, can_change_status)

        User.objects.create_user(
            'not_permited', 'not_permited@temp.com', 'not_permited')


class MoneyListTestCase(TestMixinSetUp, TestCase, TestDataMoney):

    def test_list_not_authenticate(self):
        response = self.client.get(reverse("money:moneys"))
        self.assertEqual(response.status_code, 302)

    def test_list_empty(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.get(reverse("money:moneys"))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [])

    def test_list_money(self):
        self.client.login(username='temporary', password='temporary')
        Money.objects.create(**self.money_data)
        response = self.client.get(reverse("money:moneys"))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [self.money_data])

    def test_list_not_permited_user(self):
        self.client.login(username='not_permited', password='not_permited')
        response = self.client.get(reverse("money:moneys"))
        self.assertEqual(response.status_code, 302)


class MoneyCreateTestCase(TestMixinSetUp, TestCase, TestDataMoney):

    def test_create_money_not_authenticate(self):
        response = self.client.post(
            reverse("money:create_money"), self.money_data)
        self.assertEqual(response.status_code, 302)

    def test_create_money_without_symbol(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.post(
            reverse("money:create_money"), self.money_data_without_symbol)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), self.symbol_obligatorie)

    def test_create_money_without_name(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.post(
            reverse("money:create_money"), self.money_data_without_name)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), self.name_obligatorie)

    def test_create_money_without_parameters(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.post(
            reverse("money:create_money"), {})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), self.obligatories)

    def test_create_money(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.post(
            reverse("money:create_money"), self.money_data)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [self.money_data])

    def test_create_money_without_permission(self):
        self.client.login(username='not_permited', password='not_permited')
        response = self.client.post(
            reverse("money:create_money"), self.money_data)
        self.assertEqual(response.status_code, 302)


class MoneyUpdateTestCase(TestMixinSetUp, TestCase, TestDataMoney):

    def test_update_money_not_authenticate(self):

        response = self.client.post(reverse(
            "money:update_money", kwargs=self.parameters),
            self.money_data_update)
        self.assertEqual(response.status_code, 302)

    def test_update_money(self):
        self.client.login(username='temporary', password='temporary')
        url_post = reverse(
            "money:update_money", kwargs=self.parameters)
        response = self.client.post(url_post, self.money_data_update)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), [self.money_data_update])

    def test_update_money_without_symbol(self):
        self.client.login(username='temporary', password='temporary')
        url_post = reverse(
            "money:update_money", kwargs=self.parameters)
        response = self.client.post(url_post, self.money_data_without_symbol)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), self.symbol_obligatorie)

    def test_update_money_without_name(self):
        self.client.login(username='temporary', password='temporary')
        url_post = reverse(
            "money:update_money", kwargs=self.parameters)
        response = self.client.post(url_post, self.money_data_without_name)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            force_text(response.content), self.name_obligatorie)


class MoneyDeleteTestCase(TestMixinSetUp, TestCase, TestDataMoney):

    def test_delete_money_not_authenticate(self):
        response = self.client.post(reverse(
            "money:delete_money", kwargs={'money_id': 1}))
        self.assertEqual(response.status_code, 302)

    def test_delete_money(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.post(reverse(
            "money:update_money", kwargs={'money_id': 1}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("money:moneys"))
        self.assertJSONEqual(
            force_text(response.content), [])
