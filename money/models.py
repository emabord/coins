# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from uuid import uuid4


class MoneyManager(models.Manager):

    def get_queryset(self, *args, **kwargs):
        return super(MoneyManager, self).get_queryset(
            *args, **kwargs).filter(active=True)


class Money(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    name = models.CharField(_("nombre"), max_length=100)
    symbol = models.CharField(_("símbolo"), max_length=4)
    active = models.BooleanField(_("activo"), default=True)

    objects = MoneyManager()

    class Meta:
        verbose_name = "Moneda"
        verbose_name_plural = "Monedas"

        permissions = (
            ("view_money_list", "Can see available moneys"),
            ("change_money_status", "Can change the status of moneys"),
        )

    def __str__(self):
        return self.name

    def as_json(self, parameter=None):
        return {
            "name": self.name,
            "symbol": self.symbol,
        }

    def as_json_datatable(self, parameter=None):
        return [
            self.name,
            self.symbol
        ]

    def delete(self, *args, **kwargs):
        self.active = False
        super(Money, self).save(*args, **kwargs)


class Wallet(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(_("Nombre"), max_length=200)
    money = models.ForeignKey(Money, verbose_name="Moneda")
    credit = models.DecimalField(decimal_places=20, max_digits=30)
    owner = models.ForeignKey(User, verbose_name="Propietario")

    def as_json(self, parameter=None):
        return {
            "name": self.name,
            "money": self.money.as_json(),
            "credit": str(self.credit),
            "owner": self.owner.username,
        }


class Operation(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    from_wallet = models.ForeignKey(
        Wallet, verbose_name="Billetera Origen", related_name="from_wallet")
    to_wallet = models.ForeignKey(
        Wallet, verbose_name="Billetera Destino", related_name="to_wallet")
    amount = models.DecimalField(decimal_places=20, max_digits=30)

    def as_json(self, parameter=None):
        return {
            "created": self.created.strftime("%Y-%m-%d %H:%M:%S"),
            "from_wallet": self.from_wallet.as_json(),
            "to_wallet": self.to_wallet.as_json(),
            "amount": str(self.amount)
        }
