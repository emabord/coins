from django import forms
from .models import Money


class MoneyCreateForm(forms.ModelForm):

    class Meta:
        model = Money
        fields = ['name', 'symbol', ]


class MoneyUpdateForm(MoneyCreateForm):

    def __init__(self, *args, **kwargs):
        super(MoneyUpdateForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['symbol'].required = True
