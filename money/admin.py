# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Money


class MoneyAdmin(admin.ModelAdmin):
    """
        Admin for moneys
    """
    list_display = ('name', 'symbol', 'active',)
    list_editable = ('active',)
    list_filter = ('active',)
    search_fields = ['name', ]

    def get_queryset(self, request):
        qs = self.model.admin_objects.get_queryset()
        ordering = self.ordering or ()
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


admin.site.register(Money, MoneyAdmin)
