# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from coin.datatables import DatatableAsync
from coin.serializers import JSONResponseMixin
from django.views.generic.list import ListView

from .models import Money
from .forms import MoneyCreateForm
from .forms import MoneyUpdateForm


@method_decorator(login_required, name='dispatch')
@method_decorator(
    permission_required('money.view_money_list'), name='dispatch')
class MoneyListJson(JSONResponseMixin):
    model = Money


@method_decorator(login_required, name='dispatch')
@method_decorator(
    permission_required('money.change_money_status'), name='dispatch')
class MoneyCreateJson(JSONResponseMixin):
    model = Money
    form_class = MoneyCreateForm


@method_decorator(login_required, name='dispatch')
@method_decorator(
    permission_required('money.change_money_status'), name='dispatch')
class MoneyUpdateJson(JSONResponseMixin):
    model = Money
    form_class = MoneyUpdateForm


@method_decorator(login_required, name='dispatch')
@method_decorator(
    permission_required('money.change_money_status'), name='dispatch')
class MoneyDeleteJson(JSONResponseMixin):
    model = Money

    def post(self, request, *args, **kwargs):
        pk = kwargs["money_id"]
        money = get_object_or_404(self.model, pk=pk)
        money.delete()
        return self.send_response([])


class MoneyListView(ListView):
    model = Money


"""
@method_decorator(login_required, name='dispatch')
@method_decorator(
    permission_required('money.view_money_list'), name='dispatch')
"""
class MoneyListDatatableJson(JSONResponseMixin):

    method_json = "as_json_datatable"

    def get(self, request, *args, **kwargs):

        datatable = DatatableAsync(
            Money,
            fields_orders=[
                'name',
                'symbol',
            ],
            fields_filters=[
                'name',
                'symbol',
            ],
            method_json=self.method_json
        )

        return datatable.get_result(request)
