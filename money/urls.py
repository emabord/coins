from django.conf.urls import url
from .views import MoneyListJson
from .views import MoneyCreateJson
from .views import MoneyUpdateJson
from .views import MoneyDeleteJson
from .views import MoneyListView
from .views import MoneyListDatatableJson

app_name = 'money'

urlpatterns = [

    url(
        r'moneys/$',
        MoneyListJson.as_view(),
        name="moneys"),

    url(
        r'create_money/$',
        MoneyCreateJson.as_view(),
        name="create_money"),

    url(
        r'update_money/(?P<money_id>[0-9]+)/$',
        MoneyUpdateJson.as_view(),
        name="update_money"),

    url(
        r'delete_money/(?P<money_id>[0-9]+)/$',
        MoneyDeleteJson.as_view(),
        name="delete_money"),

    url(
        r'moneys-index/$',
        MoneyListView.as_view(),
        name="moneys_index"),

    url(
        r'moneys-datatables-list/$',
        MoneyListDatatableJson.as_view(),
        name="moneys_datatables_list"),

]
